# ENSAPAY Impayes Service

Impayes Service is a service for managing bills. It allows external services to submit their bills, hence to be sent to the user.  

## Requirments
Please install these requirements before setting up the porject on your local machine :

| Requirement | DESCRIPTION                                                                         |
| ----------- | ----------------------------------------------------------------------------------- |
| JDK 8       | At least JDK 8 is required to compile the services                                  |
| Maven       | Java application packaging system   |
| MongoDB  | NoSQL Database                           |
| Consul    | Discovery and config server for services |
| Docker    | For deploying in Docker |

## Documentation

All APIs are documented with Swagger, just navigate to:
```sh
http://localhost:8095/swagger-ui/
```
## SOAP Web Services

- In addition to REST APIs, the service provides also SOAP Webservices endpoint. Check out `factures.xsd` for more details about the webservices. 
- To build java classes from the **xsd** file, run maven plugin command
 ```sh
mvn jaxb:xjc
```
Endpoint is exposed at `/ws`
## Deloyment
To deploy in Docker, just run `deploy.sh` script.

## Author

LAHMIDI Oussama
