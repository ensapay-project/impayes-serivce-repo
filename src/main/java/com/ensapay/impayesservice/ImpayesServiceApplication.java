package com.ensapay.impayesservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImpayesServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImpayesServiceApplication.class, args);
	}

}
