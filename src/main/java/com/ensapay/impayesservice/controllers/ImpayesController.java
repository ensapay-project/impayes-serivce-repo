package com.ensapay.impayesservice.controllers;

//import com.ensapay.impayesservice.entities.Facture;
import com.ensapay.impayesservice.repositories.FactureRepository;
import com.ensapay.impayesservice.services.ImpayesService;
import impayes_service.*;
import lombok.AllArgsConstructor;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/factures")
@AllArgsConstructor
public class ImpayesController {

    ImpayesService service;

    @GetMapping("{creanceId}")
    public List<Facture> getAllFactures(@PathVariable int creanceId) {
        GetAllFacturesRequest request = new GetAllFacturesRequest();
        request.setCreanceId(creanceId);
        return service.getAllFactures(request).getFactures();
    }

    @GetMapping("{creanceId}/{factureId}")
    public Facture getFacture(@PathVariable int creanceId, @PathVariable String factureId, HttpServletRequest request) {
        return getAllFactures(creanceId).stream()
            .filter(facture -> facture.getId().equals(factureId))
            .findFirst()
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }
//
    @PostMapping("{creanceId}")
    public ResponseEntity<Facture> createFacture(@PathVariable int creanceId, @RequestBody FactureRequestBody facture) {
        CreateFactureRequest request = new CreateFactureRequest();
        facture.setCreanceId(creanceId);
        request.setRequestBody(facture);
        return ResponseEntity.ok(service.createFacture(request).getFacture());
    }
//
    @PutMapping("{factureId}")
    public ResponseEntity<Facture> modifierFacture(@PathVariable String factureId, @RequestBody FactureUpdateRequestBody requestBody) {
        UpdateFactureRequest request = new UpdateFactureRequest();
        requestBody.setId(factureId);
        request.setRequestBody(requestBody);
        return ResponseEntity.ok(service.modifierFacture(request).getFacture());
    }
//
    @DeleteMapping("{factureId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteFacture(@PathVariable String factureId) {
        DeleteFactureByIdRequest request = new DeleteFactureByIdRequest();
        request.setId(factureId);
        service.deleteFacture(request);
    }
//
//    @PostMapping("{creanceId}/{factureId}/payer")
//    public ResponseEntity<Facture> payerFacture(@PathVariable int creanceId, @PathVariable String factureId, @RequestBody FactureRequestBody requestBody, HttpServletRequest request) {
//        Facture factureAPayer = getFacture(creanceId, factureId, request);
//        String currentUserId = request.getUserPrincipal().getName();
//
//        if (!currentUserId.equals(factureAPayer.getUserId()))
//            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
//
//        if (factureAPayer.isPayee())
//            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Facture deja payee");
//
//        Double montant = requestBody.getMontant();
//        if (montant == null || montant < factureAPayer.getMontantFinal())
//            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Montant invalide");
//
//        // [TODO] Communication avec service de paiement
//
//        factureAPayer.setPayee(true);
//
//        return ResponseEntity.ok(repository.save(factureAPayer));
//    }


}
