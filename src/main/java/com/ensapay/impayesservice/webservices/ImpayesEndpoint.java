package com.ensapay.impayesservice.webservices;

import com.ensapay.impayesservice.repositories.FactureRepository;
import com.ensapay.impayesservice.services.ImpayesService;
import impayes_service.*;
import lombok.AllArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import javax.servlet.http.HttpServletRequest;

@Endpoint
@AllArgsConstructor
public class ImpayesEndpoint {
    private final static String NAMESPACE_URL = "http://impayes-service";

    ImpayesService service;

    @PayloadRoot(namespace = NAMESPACE_URL, localPart = "getAllFacturesRequest")
    @ResponsePayload
    public GetAllFacturesResponse getAllFactures(@RequestPayload GetAllFacturesRequest request) {
        return service.getAllFactures(request);
    }

    @PayloadRoot(namespace = NAMESPACE_URL, localPart = "createFactureRequest")
    @ResponsePayload
    public CreateFactureResponse createFacture(@RequestPayload CreateFactureRequest request) {
        return service.createFacture(request);
    }

    @PayloadRoot(namespace = NAMESPACE_URL, localPart = "updateFactureRequest")
    @ResponsePayload
    public CreateFactureResponse updateFacture(@RequestPayload UpdateFactureRequest request) {
        return service.modifierFacture(request);
    }

    @PayloadRoot(namespace = NAMESPACE_URL, localPart = "deleteFactureByIdRequest")
    @ResponsePayload
    @Secured("ROLE_agent")
    public void deleteFacture(@RequestPayload DeleteFactureByIdRequest request) {
        service.deleteFacture(request);
    }
}
