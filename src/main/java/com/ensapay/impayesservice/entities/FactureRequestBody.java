package com.ensapay.impayesservice.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FactureRequestBody {
    private String description;
    private Double montant;
    private LocalDate dateLimit;
    private Double fraisPenalite;
    private String userId;

}
