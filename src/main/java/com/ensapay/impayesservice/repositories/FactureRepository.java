package com.ensapay.impayesservice.repositories;

import impayes_service.Facture;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface FactureRepository extends MongoRepository<Facture, String> {
    List<Facture> findAllByCreanceId(int creanceId);
    Optional<Facture> findByCreanceIdAndId(int creanceId, String id);
}
