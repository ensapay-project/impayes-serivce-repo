package com.ensapay.impayesservice.services;

import com.ensapay.impayesservice.repositories.FactureRepository;
import impayes_service.*;
import lombok.AllArgsConstructor;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ImpayesService {

    FactureRepository repository;

    public GetAllFacturesResponse getAllFactures(GetAllFacturesRequest request) {
        KeycloakAuthenticationToken principal = (KeycloakAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        GetAllFacturesResponse response = new GetAllFacturesResponse();

        List<impayes_service.Facture> factures = repository.findAllByCreanceId(request.getCreanceId());
        if (principal.getAccount().getRoles().contains("ROLE_agent"))
            response.getFactures().addAll(factures);
        else response.getFactures().addAll(
            factures.stream().filter(facture -> facture.getUserId().equals(principal.getName())).collect(Collectors.toList())
        );
        return response;
    }


    public CreateFactureResponse createFacture(CreateFactureRequest request) {
        Facture facture = new Facture();
        FactureRequestBody requestBody = request.getRequestBody();
        facture.setDateLimit(requestBody.getDateLimit());
        facture.setDescription(requestBody.getDescription());
        facture.setUserId(requestBody.getUserId());
        facture.setFraisPenalite(requestBody.getFraisPenalite());
        facture.setMontant(requestBody.getMontant());
        facture.setCreanceId(requestBody.getCreanceId());
        CreateFactureResponse response = new CreateFactureResponse();
        response.setFacture(repository.save(facture));
//        response.getFactures().addAll(repository.findAllByCreanceId(request.getCreanceId()));
        return response;
    }


    public CreateFactureResponse modifierFacture(UpdateFactureRequest request) {
        FactureUpdateRequestBody requestBody = request.getRequestBody();
        Facture facture = repository.findById(requestBody.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Facture introuvable."));
        if (requestBody.getDateLimit() != null)
            facture.setDateLimit(requestBody.getDateLimit());

        if (requestBody.getDescription() != null)
            facture.setDescription(requestBody.getDescription());

        if (requestBody.getFraisPenalite() > 0)
            facture.setFraisPenalite(requestBody.getFraisPenalite());

        if (requestBody.getMontant() > 0)
            facture.setMontant(requestBody.getMontant());
        CreateFactureResponse response = new CreateFactureResponse();
        response.setFacture(repository.save(facture));
        return response;
    }


    public void deleteFacture(DeleteFactureByIdRequest request) {
        repository.delete(
            repository.findById(request.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND))
        );
    }

}
